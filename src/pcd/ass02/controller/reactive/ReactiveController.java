package pcd.ass02.controller.reactive;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.reactivex.rxjava3.subscribers.ResourceSubscriber;
import pcd.ass02.controller.Controller;
import pcd.ass02.controller.LinkUtils;
import pcd.ass02.Launcher;
import pcd.ass02.controller.AbstractController;
import pcd.ass02.model.PageNode;

public class ReactiveController extends AbstractController implements Controller {

	private long startTime = 0L;
	private final PageNode startNode;
	private final int depthLevel;
	private boolean isComplete = false;

	public ReactiveController(PageNode startNode, int depthLevel) {
		super();
		this.startNode = startNode;
		this.depthLevel = depthLevel;
		view.get().initView(this);
	}

	@Override
	public void compute() {
		display();
		resetStartTime();
		createChildren(startNode, 0);
	}

	private void checkGraph() {
		resetStartTime();
		checkChildren(startNode, 0);
	}

	private void createChildren(PageNode node, int currentLevel) {
		Flowable<PageNode> flowable = createBranch(node, currentLevel).observeOn(Schedulers.computation());
		flowable.subscribe(new ResourceSubscriber<PageNode>() {

			@Override
			public void onNext(PageNode t) {
				vertices.add(t);
				view.get().addVertex(t.getTitle());
				if (t.getFather().isPresent()) {
					view.get().addEdge(t.getFather().get().getTitle(), t.getTitle());
				}
			}

			@Override
			public void onError(Throwable t) {
				t.printStackTrace();
				System.exit(-1);
			}

			@Override
			public void onComplete() {
				if (!isComplete) {
					isComplete = true;
					view.get().setCreated(System.currentTimeMillis() - startTime);
					checkGraph();
				}
			}

		});
	}

	private Flowable<PageNode> createBranch(PageNode link, int currentLevel) {
		Flowable<PageNode> ofLink = Flowable.just(link);
		if (currentLevel == depthLevel) {
			return ofLink;
		} else {
			return Flowable.merge(ofLink, Flowable.fromIterable(LinkUtils.getLinks(link)))
					.subscribeOn(Schedulers.computation()).flatMap(e -> createBranch(e, currentLevel + 1));
		}
	}

	private void removeChildren(PageNode node, int currentLevel) {
		// Doesn't delete vertex if it has multiple fathers
		Flowable<PageNode> flowable = Flowable.fromIterable(vertices).observeOn(Schedulers.computation());
		flowable.subscribe(new ResourceSubscriber<PageNode>() {

			@Override
			public void onNext(PageNode t) {
				view.get().removeEdge(node.getTitle(), t.getTitle()); // Edges to children
				if (Collections.frequency(vertices, t) == 1) {
					if (!node.getTitle().equals(startNode.getTitle())) {
						view.get().removeVertex(node.getTitle());
						vertices.remove(node);
					}
				}
			}

			@Override
			public void onError(Throwable t) {
				t.printStackTrace();
				System.exit(-1);
			}

			@Override
			public void onComplete() {
				if (currentLevel <= depthLevel) {
					final List<PageNode> childsChildren = vertices.parallelStream().filter(
							e -> e.getFather().isPresent() && e.getFather().get().getTitle().equals(node.getTitle()))
							.collect(Collectors.toList());

					Flowable<PageNode> flowable = Flowable.fromIterable(childsChildren).observeOn(Schedulers.computation());

					
					flowable.subscribe(e -> {
						removeChildren(e, currentLevel + 1);
					});
				}
			}

		});
	}

	private void checkChildren(PageNode node, int currentLevel) {

		if (currentLevel < depthLevel) {

			final Set<PageNode> currentChildren = vertices.parallelStream()
					.filter(e -> e.getFather().isPresent() && e.getFather().get().getTitle().equals(node.getTitle()))
					.collect(Collectors.toSet());

			final Set<PageNode> newChildren = LinkUtils.getLinks(node);

			if (Launcher.ALTER_LINK_SET && currentLevel == 0) {
				newChildren.clear();
				newChildren.add(new PageNode("Faenza", Optional.of(node)));
				newChildren.add(new PageNode("Cesena", Optional.of(node)));
				newChildren.add(new PageNode("Bologna", Optional.of(node)));
				newChildren.add(new PageNode("Ravenna", Optional.of(node)));
				newChildren.add(new PageNode("Forl�", Optional.of(node)));
			}

			final Set<PageNode> toRemove = currentChildren.parallelStream().filter(e -> !newChildren.contains(e))
					.collect(Collectors.toSet());

			final Set<PageNode> toAdd = newChildren.parallelStream().filter(e -> !currentChildren.contains(e))
					.collect(Collectors.toSet());

			final Set<PageNode> toCheck = new HashSet<>(currentChildren);
			toCheck.removeAll(toRemove);

			Flowable<PageNode> toRemoveFlowable = Flowable.fromIterable(toRemove).observeOn(Schedulers.computation());
			toRemoveFlowable.subscribe(new ResourceSubscriber<PageNode>() {

				@Override
				public void onNext(PageNode t) {
					view.get().removeEdge(node.getTitle(), t.getTitle());
					removeChildren(t, currentLevel);
				}

				@Override
				public void onError(Throwable t) {
					t.printStackTrace();
					System.exit(-1);
				}

				@Override
				public void onComplete() {
					Flowable<PageNode> toAddFlowable = Flowable.fromIterable(toAdd).observeOn(Schedulers.computation());
					toAddFlowable.subscribe(new ResourceSubscriber<PageNode>() {

						@Override
						public void onNext(PageNode t) {
							vertices.add(t);
							createChildren(t, currentLevel + 1);
						}

						@Override
						public void onError(Throwable t) {
							t.printStackTrace();
							System.exit(-1);
						}

						@Override
						public void onComplete() {
							Flowable<PageNode> toCheckFlowable = Flowable.fromIterable(toCheck)
									.observeOn(Schedulers.computation());
							toCheckFlowable.subscribe(new ResourceSubscriber<PageNode>() {

								@Override
								public void onNext(PageNode t) {
									checkChildren(t, currentLevel + 1);
								}

								@Override
								public void onError(Throwable t) {
									t.printStackTrace();
									System.exit(-1);
								}

								@Override
								public void onComplete() {
									view.get().setUpdated(System.currentTimeMillis() - startTime);
									checkGraph();
								}

							});
						}

					});
				}

			});

		}

	}

	@Override
	public String getParadigm() {
		return Controller.Paradigm.REACT.toString();
	}

	private void resetStartTime() {
		startTime = System.currentTimeMillis();
	}

}