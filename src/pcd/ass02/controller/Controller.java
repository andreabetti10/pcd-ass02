package pcd.ass02.controller;

public interface Controller {
	
	public void display();

	public void compute();

	public void handleClosure();
	
	public enum Paradigm {
		TASK {
			@Override
			public String toString() {
				return "Tasks version";
			}
		},
		ASYNC {
			@Override
			public String toString() {
				return "Async version";
			}
		},
		REACT {
			@Override
			public String toString() {
				return "Reactive version";
			}
		}
	}

	public String getParadigm();

}
