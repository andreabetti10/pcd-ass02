package pcd.ass02.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Optional;

import pcd.ass02.controller.Controller.Paradigm;
import pcd.ass02.controller.async.AsyncController;
import pcd.ass02.controller.reactive.ReactiveController;
import pcd.ass02.controller.task.TaskController;
import pcd.ass02.model.PageNode;
import pcd.ass02.view.StartView;

public class StartListener implements ActionListener {

	private final StartView startPanel = new StartView(this);

	@Override
	public void actionPerformed(ActionEvent e) {
		new Thread(() -> {
			final Paradigm paradigm = startPanel.getSelectedParadigm();
			final int depthLevel = startPanel.getDepthLevel();
			final PageNode startPage = new PageNode(startPanel.getStartPageTextField(), Optional.empty());
			switch (paradigm) {
			case TASK:
				new TaskController(startPage, depthLevel).compute();
				break;
			case ASYNC:
				new AsyncController(startPage, depthLevel).compute();
				break;
			case REACT:
				new ReactiveController(startPage, depthLevel).compute();
				break;
			}
		}).start();

	}

}
