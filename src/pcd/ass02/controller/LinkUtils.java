package pcd.ass02.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pcd.ass02.model.PageNode;

public class LinkUtils {

	private final static String LINKS_URL = "https://it.wikipedia.org/w/api.php?action=parse&format=json&section=0&prop=links&page=";

	public static Set<PageNode> getLinks(PageNode page) {
		final Set<PageNode> links = new HashSet<>();
		try {
			final JSONObject response = readJsonFromUrl(LINKS_URL + URLEncoder.encode(page.getTitle(), "UTF-8"));
			final JSONArray linksArray = response.getJSONObject("parse").getJSONArray("links");
			for (int i = 0; i < linksArray.length(); ++i) {
				final JSONObject link = linksArray.getJSONObject(i);
				if (link.getInt("ns") == 0) {
					links.add(new PageNode(link.getString("*"), Optional.of(page)));
				}
			}
		} catch (final JSONException e) {
			// ignored JSON, not links format
		} catch (final UnsupportedEncodingException e) {
			e.printStackTrace();
			System.exit(-1);
		}

		return links;
	}

	private static JSONObject readJsonFromUrl(String url) {
		InputStream is = null;
		try {
			is = new URL(url).openStream();
		} catch (final MalformedURLException e) {
			e.printStackTrace();
			System.exit(-1);
		} catch (final IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		try {
			final BufferedReader rd = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			final String jsonText = readAll(rd);
			final JSONObject json = new JSONObject(jsonText);
			return json;
		} catch (final JSONException e) {
			e.printStackTrace();
			System.exit(-1);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			System.exit(-1);
		} finally {
			try {
				is.close();
			} catch (final IOException e) {
				e.printStackTrace();
				System.exit(-1);
			}
		}
		return null;
	}

	private static String readAll(Reader rd) {
		final StringBuilder sb = new StringBuilder();
		int cp;
		try {
			while ((cp = rd.read()) != -1) {
				sb.append((char) cp);
			}
		} catch (final IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return sb.toString();
	}

}
