package pcd.ass02.controller.async;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import pcd.ass02.model.PageNode;
import pcd.ass02.view.ControllableView;

public class PageRemoverVerticle extends AbstractVerticle {

	private final Vertx vertx;
	private final Promise<PageNode> pagePromise;
	private final PageNode page;
	private final List<Promise<PageNode>> childrenPromises = new ArrayList<>();
	private final int currentLevel;
	private final int depthLevel;
	private final List<PageNode> vertices;
	private final Optional<ControllableView> view;
	
	public PageRemoverVerticle(Vertx vertx, Promise<PageNode> pagePromise, PageNode page,
			int previousLevel, int depthLevel, List<PageNode> vertices, Optional<ControllableView> view) {
		this.vertx = vertx;
		this.pagePromise = pagePromise;
		this.page = page;
		currentLevel = previousLevel + 1;
		this.depthLevel = depthLevel;
		this.vertices = vertices;
		this.view = view;
	}
	
	@Override
	public void start() {
		// Doesn't delete vertex if it has multiple fathers
		final String origin = vertices.get(0).getTitle();
		vertices.forEach(e -> {
			view.get().removeEdge(page.getTitle(), e.getTitle()); // Edges to children
			if (Collections.frequency(vertices, e) == 1) {
				if (!page.getTitle().equals(origin)) {
					view.get().removeVertex(page.getTitle());
					vertices.remove(page);
				}
			}
		});

		if (currentLevel <= depthLevel) {
			final List<PageNode> childsChildren = vertices.parallelStream()
					.filter(e -> e.getFather().isPresent() && e.getFather()
							.get().getTitle().equals(page.getTitle()))
					.collect(Collectors.toList());

			childsChildren.forEach(e -> {
				final Promise<PageNode> linkPromise = Promise.promise();
				childrenPromises.add(linkPromise);
				vertx.deployVerticle(new PageRemoverVerticle(vertx, linkPromise,
						e, currentLevel, depthLevel, vertices, view));
			});
			
			CompositeFuture
					.all(childrenPromises.parallelStream().map(p -> p.future())
							.collect(Collectors.toList()))
					.onComplete(res -> pagePromise.complete(page));
		} else {
			pagePromise.complete(page);
		}
	}
	
}