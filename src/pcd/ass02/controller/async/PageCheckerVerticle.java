package pcd.ass02.controller.async;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import pcd.ass02.Launcher;
import pcd.ass02.controller.LinkUtils;
import pcd.ass02.model.PageNode;
import pcd.ass02.view.ControllableView;

public class PageCheckerVerticle extends AbstractVerticle {

	private final Vertx vertx;
	private final Promise<PageNode> pagePromise;
	private final PageNode page;
	private final List<Promise<PageNode>> childrenPromises = new ArrayList<>();
	private final int currentLevel;
	private final int depthLevel;
	private final List<PageNode> vertices;
	private final Optional<ControllableView> view;

	public PageCheckerVerticle(Vertx vertx, Promise<PageNode> pagePromise, PageNode page, int previousLevel,
			int depthLevel, List<PageNode> vertices, Optional<ControllableView> view) {
		this.vertx = vertx;
		this.pagePromise = pagePromise;
		this.page = page;
		currentLevel = previousLevel + 1;
		this.depthLevel = depthLevel;
		this.vertices = vertices;
		this.view = view;
	}

	@Override
	public void start() {
		if (currentLevel <= depthLevel) {
			final Set<PageNode> currentChildren = vertices.stream()
					.filter(e -> e.getFather().isPresent() && e.getFather().get().getTitle().equals(page.getTitle()))
					.collect(Collectors.toSet());

			final Set<PageNode> newChildren = LinkUtils.getLinks(page);

			if (Launcher.ALTER_LINK_SET && currentLevel == 1) {
				newChildren.clear();
				newChildren.add(new PageNode("Faenza", Optional.of(page)));
				newChildren.add(new PageNode("Cesena", Optional.of(page)));
				newChildren.add(new PageNode("Bologna", Optional.of(page)));
				newChildren.add(new PageNode("Ravenna", Optional.of(page)));
				newChildren.add(new PageNode("Forl�", Optional.of(page)));
			}

			final Set<PageNode> toRemove = currentChildren.parallelStream().filter(e -> !newChildren.contains(e))
					.collect(Collectors.toSet());

			final Set<PageNode> toAdd = newChildren.parallelStream().filter(e -> !currentChildren.contains(e))
					.collect(Collectors.toSet());

			final Set<PageNode> toCheck = new HashSet<>(currentChildren);
			toCheck.removeAll(toRemove);

			toRemove.forEach(link -> {
				if (view.isPresent()) {
					view.get().removeEdge(page.getTitle(), link.getTitle());
				}
				final Promise<PageNode> removePromise = Promise.promise();
				childrenPromises.add(removePromise);
				vertx.deployVerticle(
						new PageRemoverVerticle(vertx, removePromise, link, currentLevel, depthLevel, vertices, view));
			});

			toAdd.forEach(link -> {
				final Promise<PageNode> addPromise = Promise.promise();
				childrenPromises.add(addPromise);
				vertices.add(link);
				if (view.isPresent()) {
					view.get().addVertex(link.getTitle());
					view.get().addEdge(link.getFather().get().getTitle(), link.getTitle());
				}
				vertx.deployVerticle(
						new PageCreatorVerticle(vertx, addPromise, link, currentLevel, depthLevel, vertices, view));
			});

			toCheck.forEach(link -> {
				final Promise<PageNode> checkPromise = Promise.promise();
				childrenPromises.add(checkPromise);
				vertx.deployVerticle(
						new PageCheckerVerticle(vertx, checkPromise, link,
								currentLevel, depthLevel, vertices, view));
			});

			CompositeFuture.all(childrenPromises.parallelStream().map(p -> p.future()).collect(Collectors.toList()))
					.onComplete(res -> pagePromise.complete(page));

		} else {
			pagePromise.complete(page);
		}
	}
}
