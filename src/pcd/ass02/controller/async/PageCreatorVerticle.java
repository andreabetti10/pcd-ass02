package pcd.ass02.controller.async;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import pcd.ass02.controller.LinkUtils;
import pcd.ass02.model.PageNode;
import pcd.ass02.view.ControllableView;

public class PageCreatorVerticle extends AbstractVerticle {

	private final Vertx vertx;
	private final Promise<PageNode> pagePromise;
	private final PageNode page;
	private final List<Promise<PageNode>> childrenPromises = new ArrayList<>();
	private final int currentLevel;
	private final int depthLevel;
	private final List<PageNode> vertices;
	private final Optional<ControllableView> view;
	
	public PageCreatorVerticle(Vertx vertx, Promise<PageNode> pagePromise,
			PageNode page,
			int previousLevel, int depthLevel, List<PageNode> vertices,
			Optional<ControllableView> view) {
		this.vertx = vertx;
		this.pagePromise = pagePromise;
		this.page = page;
		currentLevel = previousLevel + 1;
		this.depthLevel = depthLevel;
		this.vertices = vertices;
		this.view = view;
	}
	
	@Override
	public void start() {
		if (currentLevel <= depthLevel) {
			for (final PageNode link : LinkUtils.getLinks(page)) {
				final Promise<PageNode> linkPromise = Promise.promise();
				childrenPromises.add(linkPromise);
				vertices.add(link);
				if (view.isPresent()) {
					view.get().addVertex(link.getTitle());
					view.get().addEdge(link.getFather().get().getTitle(), link.getTitle());
				}
				vertx.deployVerticle(new PageCreatorVerticle(vertx, linkPromise,
						link, currentLevel, depthLevel, vertices, view));
			}
			CompositeFuture
					.all(childrenPromises.parallelStream().map(p -> p.future())
							.collect(Collectors.toList()))
					.onComplete(res -> pagePromise.complete(page));
		} else {
			pagePromise.complete(page);
		}
	}
	
}
