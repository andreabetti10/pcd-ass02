package pcd.ass02.controller.async;

import java.util.Optional;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import pcd.ass02.controller.AbstractController;
import pcd.ass02.controller.Controller;
import pcd.ass02.model.PageNode;

public class AsyncController extends AbstractController implements Controller {

	private final static int NUM_THREADS = Runtime.getRuntime().availableProcessors() + 1;

	private long startTime = 0L;
	private Optional<Vertx> vertx = Optional.empty();
	private Promise<PageNode> startPromise = Promise.promise();
	private final PageNode startNode;
	private final int depthLevel;

	public AsyncController(PageNode startNode, int depthLevel) {
		super();
		this.startNode = startNode;
		this.depthLevel = depthLevel;
		view.get().initView(this);
		final VertxOptions options = new VertxOptions();
		options.setMaxEventLoopExecuteTime(Long.MAX_VALUE);
		options.setWorkerPoolSize(NUM_THREADS);
		vertx = Optional.of(Vertx.vertx(options));
	}

	@Override
	public void compute() {
		display();
		createGraph();
	}

	private void createGraph() {
		resetStartTime();
		vertices.add(startNode);
		if (view.isPresent()) {
			view.get().addVertex(startNode.getTitle());
		}
		if (vertx.isPresent()) {
			vertx.get().deployVerticle(
					new PageCreatorVerticle(vertx.get(), startPromise, startNode,
							0, depthLevel, vertices, view));
		}
		startPromise.future().onComplete(res -> {
			view.get().setCreated(System.currentTimeMillis() - startTime);
			resetStartTime();
			checkGraph();
		});
	}
	
	private void checkGraph() {
		startPromise = Promise.promise();
		if (vertx.isPresent()) {
			vertx.get().deployVerticle(
					new PageCheckerVerticle(vertx.get(), startPromise, startNode,
							0, depthLevel, vertices, view));
		}
		startPromise.future().onComplete(res -> {
			view.get().setUpdated(System.currentTimeMillis() - startTime);
			resetStartTime();
			checkGraph();
		});
	}

	@Override
	public void handleClosure() {
		if (vertx.isPresent()) {
			vertx.get().close();
		}
	}

	@Override
	public String getParadigm() {
		return Controller.Paradigm.ASYNC.toString();
	}
	
	private void resetStartTime() {
		startTime = System.currentTimeMillis();
	}
}