package pcd.ass02.controller;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import org.jgrapht.Graph;
import org.jgrapht.ListenableGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultListenableGraph;

import pcd.ass02.model.PageNode;
import pcd.ass02.view.AnonymousEdge;
import pcd.ass02.view.ControllableView;
import pcd.ass02.view.JGraphXView;

public abstract class AbstractController implements Controller {
	
	private static final int REFRESH_MS = 250;
	protected Optional<ControllableView> view = Optional.empty();
	protected List<PageNode> vertices = new CopyOnWriteArrayList<>();
	protected ListenableGraph<String, AnonymousEdge> lg;
	
	public AbstractController() {
		resetGraph();
		view = Optional.of(new JGraphXView(lg));
	}
	
	protected void resetGraph() {
		final Graph<String, AnonymousEdge> g = new DefaultDirectedGraph<>(AnonymousEdge.class);
		lg = new DefaultListenableGraph<>(g);
		if (view.isPresent()) {
			view.get().setGraph(lg);
		}
	}
	
	@Override
	public void display() {
		new Thread(() -> {
			while (true) {
				try {
					Thread.sleep(REFRESH_MS);
					if (view.isPresent()) {
						view.get().display();
					}
				} catch (final InterruptedException e) {
					e.printStackTrace();
					System.exit(-1);
				}
			}
		}).start();
	}

	@Override
	public void handleClosure() {
		return;
	}

}
