package pcd.ass02.controller.task;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import pcd.ass02.model.PageNode;
import pcd.ass02.view.ControllableView;

public class RemoveChildrenTask implements Callable<Void> {

	private final PageNode page;
	private final int currentLevel;
	private final int depthLevel;
	private final ExecutorService executor;
	private final List<PageNode> vertices;
	private final Set<Future<Void>> futures;
	private final Optional<ControllableView> view;

	public RemoveChildrenTask(PageNode page, int previousLevel,
			int depthLevel, ExecutorService executor, List<PageNode> vertices2, Set<Future<Void>> futures,
			Optional<ControllableView> view) {
		this.page = page;
		currentLevel = previousLevel + 1;
		this.depthLevel = depthLevel;
		this.executor = executor;
		this.vertices = vertices2;
		this.futures = futures;
		this.view = view;
	}

	@Override
	public Void call() throws Exception {
		// Doesn't delete vertex if it has multiple fathers
		String origin = vertices.get(0).getTitle();
		vertices.forEach(e -> {
			view.get().removeEdge(page.getTitle(), e.getTitle()); // Edges to children
			if (Collections.frequency(vertices, e) == 1) {
				if (!page.getTitle().equals(origin)) {
					view.get().removeVertex(page.getTitle());
					vertices.remove(page);
				}
			}
		});

		if (currentLevel <= depthLevel) {
			final List<PageNode> childsChildren = vertices.parallelStream()
					.filter(e -> e.getFather().isPresent() && e.getFather().get().getTitle().equals(page.getTitle()))
					.collect(Collectors.toList());
			
			childsChildren.forEach(e -> {
				futures.add(executor.submit(
						new RemoveChildrenTask(e, currentLevel, depthLevel, executor, vertices, futures, view)));
			});
		}

		return null;
	}

}
