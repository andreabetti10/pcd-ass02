package pcd.ass02.controller.task;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import pcd.ass02.controller.AbstractController;
import pcd.ass02.controller.Controller;
import pcd.ass02.model.PageNode;

public class TaskController extends AbstractController implements Controller {

	private final static int NUM_THREADS = Runtime.getRuntime().availableProcessors() + 1;

	private long startTime = 0L;
	private final ExecutorService executor = Executors.newFixedThreadPool(NUM_THREADS);
	private final Set<Future<Void>> futures = ConcurrentHashMap.newKeySet();
	private final PageNode startNode;
	private final int depthLevel;

	public TaskController(PageNode startNode, int depthLevel) {
		super();
		this.startNode = startNode;
		this.depthLevel = depthLevel;
		view.get().initView(this);
	}

	@Override
	public void compute() {
		display();
		createGraph();
	}

	private void createGraph() {
		resetStartTime();
		vertices.add(startNode);
		if (view.isPresent()) {
			view.get().addVertex(startNode.getTitle());
		}
		futures.add(executor
				.submit(new CreateChildrenTask(startNode, 0, depthLevel, executor, vertices, futures, view)));
		futures.forEach(e -> {
			try {
				e.get();
			} catch (InterruptedException | ExecutionException ex) {
				ex.printStackTrace();
				System.exit(-1);
			}
		});
		futures.clear();
		view.get().setCreated(System.currentTimeMillis() - startTime);
		checkGraph();
	}

	private void checkGraph() {
		resetStartTime();
		futures.add(executor
				.submit(new CheckChildrenTask(startNode, 0, depthLevel, executor, vertices, futures, view)));
		futures.forEach(e -> {
			try {
				e.get();
			} catch (InterruptedException | ExecutionException ex) {
				ex.printStackTrace();
				System.exit(-1);
			}
		});
		futures.clear();
		view.get().setUpdated(System.currentTimeMillis() - startTime);
		checkGraph();
	}

	@Override
	public String getParadigm() {
		return Controller.Paradigm.TASK.toString();
	}

	private void resetStartTime() {
		startTime = System.currentTimeMillis();
	}

}