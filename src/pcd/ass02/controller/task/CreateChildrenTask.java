package pcd.ass02.controller.task;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import pcd.ass02.controller.LinkUtils;
import pcd.ass02.model.PageNode;
import pcd.ass02.view.ControllableView;

public class CreateChildrenTask implements Callable<Void> {

	private final PageNode page;
	private final int currentLevel;
	private final int depthLevel;
	private final ExecutorService executor;
	private final Set<Future<Void>> futures;
	private final List<PageNode> vertices;
	private final Optional<ControllableView> view;

	public CreateChildrenTask(PageNode page, int previousLevel, int depthLevel,
			ExecutorService executor, List<PageNode> vertices2,
			Set<Future<Void>> futures, Optional<ControllableView> view) {
		this.page = page;
		currentLevel = previousLevel + 1;
		this.depthLevel = depthLevel;
		this.executor = executor;
		this.vertices = vertices2;
		this.futures = futures;
		this.view = view;
	}

	@Override
	public Void call() {
		if (currentLevel <= depthLevel) {
			LinkUtils.getLinks(page).forEach(link -> {
				vertices.add(link);
				if (view.isPresent()) {
					view.get().addVertex(link.getTitle());
					view.get().addEdge(link.getFather().get().getTitle(), link.getTitle());
				}
				futures.add(executor.submit(new CreateChildrenTask(link, currentLevel, depthLevel, executor, vertices, futures, view)));
			});
		}
		return null;
	}

}
