package pcd.ass02.controller.task;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import pcd.ass02.Launcher;
import pcd.ass02.controller.LinkUtils;
import pcd.ass02.model.PageNode;
import pcd.ass02.view.ControllableView;

public class CheckChildrenTask implements Callable<Void> {

	private final PageNode page;
	private final int currentLevel;
	private final int depthLevel;
	private final ExecutorService executor;
	private final List<PageNode> vertices;
	private final Set<Future<Void>> futures;
	private final Optional<ControllableView> view;

	public CheckChildrenTask(PageNode page, int previousLevel,
			int depthLevel, ExecutorService executor, List<PageNode> vertices,
			Set<Future<Void>> futures, Optional<ControllableView> view) {
		this.page = page;
		currentLevel = previousLevel + 1;
		this.depthLevel = depthLevel;
		this.executor = executor;
		this.vertices = vertices;
		this.futures = futures;
		this.view = view;
	}

	@Override
	public Void call() throws Exception {
		if (currentLevel <= depthLevel) {
			final Set<PageNode> currentChildren = vertices.parallelStream()
					.filter(e -> e.getFather().isPresent() && e.getFather().get().getTitle().equals(page.getTitle()))
					.collect(Collectors.toSet());

			final Set<PageNode> newChildren = LinkUtils.getLinks(page);

			if (Launcher.ALTER_LINK_SET && currentLevel == 1) {
					newChildren.clear();
					newChildren.add(new PageNode("Faenza", Optional.of(page)));
					newChildren.add(new PageNode("Cesena", Optional.of(page)));
					newChildren.add(new PageNode("Bologna", Optional.of(page)));
					newChildren.add(new PageNode("Ravenna", Optional.of(page)));
					newChildren.add(new PageNode("Forl�", Optional.of(page)));
			}

			final Set<PageNode> toRemove = currentChildren.parallelStream().filter(e -> !newChildren.contains(e))
					.collect(Collectors.toSet());

			final Set<PageNode> toAdd = newChildren.parallelStream().filter(e -> !currentChildren.contains(e))
					.collect(Collectors.toSet());
			
			final Set<PageNode> toCheck = new HashSet<>(currentChildren);
			toCheck.removeAll(toRemove);

			toRemove.forEach(link -> {
				if (view.isPresent()) {
					view.get().removeEdge(page.getTitle(), link.getTitle());
				}
				futures.add(executor.submit(
						new RemoveChildrenTask(link, currentLevel, depthLevel, executor, vertices, futures, view)));
			});

			toAdd.forEach(link -> {
				vertices.add(link);
				if (view.isPresent()) {
					view.get().addVertex(link.getTitle());
					view.get().addEdge(link.getFather().get().getTitle(), link.getTitle());
				}
				futures.add(executor.submit(
						new CreateChildrenTask(link, currentLevel, depthLevel, executor, vertices, futures, view)));
			});
			
			toCheck.forEach(link -> {
				futures.add(executor.submit(
						new CheckChildrenTask(link, currentLevel, depthLevel, executor, vertices, futures, view)));
			});

		}

		return null;

	}

}
