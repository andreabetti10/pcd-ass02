package pcd.ass02.model;

import java.util.Optional;

public class PageNode {

	private final String title;
	private final Optional<PageNode> father;
	
	public PageNode(String title, Optional<PageNode> father) {
		this.father = father;
		this.title = title;
	}
	
	public String getTitle() {
		return this.title;
	}

	@Override
	public String toString() {
		return title;
	}
	
	@Override
	public int hashCode() {
		return title.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj.getClass().equals(PageNode.class) && ((PageNode) obj).getTitle().equals(title);
	}
	
	public Optional<PageNode> getFather() {
		return father;
	}
	
}