package pcd.ass02.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import pcd.ass02.controller.Controller.Paradigm;

public class StartView extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private final JButton startButton = new JButton("Start");
	private final JComboBox<Paradigm> paradigmSelector = new JComboBox<>(
			new Paradigm[] { Paradigm.TASK, Paradigm.ASYNC, Paradigm.REACT });
	private final JTextField startPageTextField = new JTextField("Imola", 50);
	private final JSpinner levelSelector = new JSpinner(new SpinnerNumberModel(1, 0, null, 1));

	public StartView(ActionListener listener) {
		setTitle("Wikipedia pages graph builder");
		setSize(1000, 100);
		setResizable(true);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent ev) {
				System.exit(-1);
			}

			@Override
			public void windowClosed(WindowEvent ev) {
				System.exit(-1);
			}
		});
		final JPanel p = new JPanel();
		p.add(paradigmSelector);
		p.add(startPageTextField);
		p.add(levelSelector);
		p.add(startButton);
		getContentPane().add(p);
		setVisible(true);
		startButton.addActionListener(this);
		startButton.addActionListener(listener);
	}

	public Paradigm getSelectedParadigm() {
		return (Paradigm) paradigmSelector.getSelectedItem();
	}

	public String getStartPageTextField() {
		return startPageTextField.getText();
	}

	public int getDepthLevel() {
		return (Integer) levelSelector.getValue();
	}

	@Override
	public void actionPerformed(ActionEvent ev) {
		startButton.setEnabled(false);
		this.setVisible(false);
	}

}