package pcd.ass02.view;

import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Optional;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.jgrapht.ListenableGraph;
import org.jgrapht.ext.JGraphXAdapter;
import com.mxgraph.layout.mxFastOrganicLayout;
import com.mxgraph.swing.mxGraphComponent;

import pcd.ass02.controller.Controller;

public class JGraphXView extends JFrame implements ControllableView {

	/**
	 * A unique serial version identifier
	 * 
	 * @see Serializable#serialVersionUID
	 */
	private static final long serialVersionUID = -2670173279978483044L;
	private static final Dimension DEFAULT_SIZE = GraphicsEnvironment.getLocalGraphicsEnvironment()
			.getMaximumWindowBounds().getSize();
	private static final String TITLE = "Wikipedia pages graph builder";
	private Optional<Controller> controller = Optional.empty();
	private JGraphXAdapter<String, AnonymousEdge> jgxAdapter;
	private final mxGraphComponent component;
	private mxFastOrganicLayout layout;
	private ListenableGraph<String, AnonymousEdge> lg;
	private String paradigm;

	public JGraphXView(ListenableGraph<String, AnonymousEdge> lg) {
		this.lg = lg;
		setTitle(TITLE);
		setPreferredSize(DEFAULT_SIZE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent ev) {
				if (controller.isPresent()) {
					controller.get().handleClosure();
				}
				System.exit(-1);
			}

			@Override
			public void windowClosed(WindowEvent ev) {
				System.exit(-1);
			}
		});
		jgxAdapter = new JGraphXAdapter<>(lg);
		component = new mxGraphComponent(jgxAdapter);
		component.setConnectable(false);
		component.getGraph().setAllowDanglingEdges(false);
		final Dimension d = new Dimension(DEFAULT_SIZE.width - 5, DEFAULT_SIZE.height - 34);
		component.setSize(d);
		component.setEnabled(false);
		getContentPane().add(component);
		layout = new mxFastOrganicLayout(jgxAdapter);
		layout.setForceConstant(150);
		layout.setMinDistanceLimit(150);
		layout.execute(jgxAdapter.getDefaultParent());
		setResizable(false);
		pack();
		setVisible(true);
	}

	@Override
	public void display() {
		try {
			SwingUtilities.invokeAndWait(() -> {
				if (controller.isPresent()) {
					synchronized (lg) {
						layout.execute(jgxAdapter.getDefaultParent());
						setTitle(TITLE + " [" + paradigm + "] [Examined nodes: "
								+ lg.vertexSet().size() + "]");
					}
				}
			});
		} catch (final Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	@Override
	public void setGraph(ListenableGraph<String, AnonymousEdge> lg) {
		synchronized (lg) {
			this.lg = lg;
			jgxAdapter = new JGraphXAdapter<>(lg);
			component.setGraph(jgxAdapter);
			layout = new mxFastOrganicLayout(jgxAdapter);
			layout.setForceConstant(150);
			layout.setMinDistanceLimit(150);
		}
	}

	@Override
	public void addVertex(String v) {
		try {
			SwingUtilities.invokeAndWait(() -> {
				synchronized (lg) {
					lg.addVertex(v);
				}
			});
		} catch (InvocationTargetException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void removeVertex(String v) {
		try {
			SwingUtilities.invokeAndWait(() -> {
				synchronized (lg) {
					lg.removeVertex(v);
				}
			});
		} catch (InvocationTargetException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addEdge(String sourceVertex, String targetVertex) {
		try {
			SwingUtilities.invokeAndWait(() -> {
				synchronized (lg) {
					if (lg.containsVertex(sourceVertex) && lg.containsVertex(targetVertex)) {
						lg.addEdge(sourceVertex, targetVertex, new AnonymousEdge());
					}
				}
			});
		} catch (InvocationTargetException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void removeEdge(String sourceVertex, String targetVertex) {
		try {
			SwingUtilities.invokeAndWait(() -> {
				synchronized (lg) {
					if (lg.containsVertex(sourceVertex) && lg.containsVertex(targetVertex)) {
						lg.removeEdge(sourceVertex, targetVertex);
					}
				}
			});
		} catch (InvocationTargetException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setCreated(long time) {
		try {
			SwingUtilities.invokeAndWait(() -> {
				System.out.println("Graph completed: " + time + "ms with " + lg.vertexSet().size() + " nodes");
			});
		} catch (InvocationTargetException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setUpdated(long time) {
		try {
			SwingUtilities.invokeAndWait(() -> {
				System.out.println("Graph updated: " + time + "ms with " + lg.vertexSet().size() + " nodes");
			});
		} catch (InvocationTargetException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void initView(Controller controller) {
		this.controller = Optional.of(controller);
		paradigm = controller.getParadigm();
	}

}
