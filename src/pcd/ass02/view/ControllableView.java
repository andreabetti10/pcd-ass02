package pcd.ass02.view;

import org.jgrapht.ListenableGraph;

import pcd.ass02.controller.Controller;

public interface ControllableView extends View {
	
	public void setGraph(ListenableGraph<String, AnonymousEdge> lg);

	public void initView(Controller controller);
	
	public void addVertex(String v);
	
	public void removeVertex(String v);
	
	public void addEdge(String sourceVertex, String targetVertex);
	
	public void removeEdge(String sourceVertex, String targetVertex);

	public void setCreated(long time);

	public void setUpdated(long time);

}